package com.tw.factory;

import com.tw.entity.ProjectEntity;

public interface CreateProjectCommand {

    void execute(ProjectEntity projectEntity);

}
