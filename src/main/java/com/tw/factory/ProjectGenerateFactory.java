package com.tw.factory;

import com.tw.entity.ProjectEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by ligen on 2017/11/6.
 */
@Component
public class ProjectGenerateFactory {

    private final SpringProjectGenerate springProjectGenerate;

    @Autowired
    public ProjectGenerateFactory(SpringProjectGenerate springProjectGenerate) {
        this.springProjectGenerate = springProjectGenerate;
    }

    public ProjectGenerate getProjectGenerate(ProjectEntity projectEntity){
        switch (projectEntity.getProjectType()){
            case JAVA_PROJECT:
                return springProjectGenerate;
            default:
                return null;
        }
    }
}
