package com.tw.factory;

import com.tw.entity.ProjectEntity;
import com.tw.entity.SpringProjectEntity;
import com.tw.enumeration.InitializerProjectShellPath;
import com.tw.exception.ErrorCode;
import com.tw.exception.RequestException;
import com.tw.util.ExecuteShell;
import org.springframework.stereotype.Service;

@Service
public class SpringProjectGenerate implements ProjectGenerate, CreateProjectCommand {

    @Override
    public void execute(ProjectEntity projectEntity) {
        SpringProjectEntity springProjectEntity = (SpringProjectEntity) projectEntity;
        String generateProjectParams = springProjectEntity.generateProjectParams();
        if (0 != ExecuteShell
                .executeShell(InitializerProjectShellPath.SPRING_SHELL_PATH.getShellPath(), generateProjectParams)) {
            throw new RequestException(ErrorCode.SYSTEM_ERROR);
        }
    }

}
