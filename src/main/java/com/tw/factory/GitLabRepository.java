package com.tw.factory;

import com.tw.entity.ProjectEntity;
import com.tw.enumeration.InitializerProjectShellPath;
import com.tw.exception.ErrorCode;
import com.tw.exception.RequestException;
import com.tw.util.ExecuteShell;
import org.springframework.stereotype.Service;

@Service
public class GitLabRepository extends GitRepository implements VcsRepository {

    @Override
    public void execute(ProjectEntity projectEntity) {
        int exitValue = ExecuteShell.executeShell(InitializerProjectShellPath.INIT_GIT_LAB_SHELL_PATH.getShellPath(), projectEntity.getName());
        if(0 != exitValue) {
            throw new RequestException(ErrorCode.SYSTEM_ERROR);
        }
        super.initRepository(projectEntity);
    }

}
