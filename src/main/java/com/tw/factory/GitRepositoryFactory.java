package com.tw.factory;

import com.tw.entity.ProjectEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class GitRepositoryFactory {

    @Autowired
    private GitLabRepository gitLabRepository;

    public VcsRepository getGitRepository(ProjectEntity projectEntity) {
        switch (projectEntity.getGitRepositoryType()) {
            case GITLAB:
                return gitLabRepository;
        }
        return null;
    }
}
