package com.tw.entity;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.util.StringUtils;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.PROTECTED_AND_PUBLIC)
public class SpringProjectEntity extends ProjectEntity {

    private String artifactId;
    private String bootVersion;
    private String build;
    private String dependencies;
    private String description;
    private String force;
    private String format;
    private String groupId;
    private String javaVersion;
    private String language;
    private String packaging;
    private String packageName;
    private String type;
    private String target;
    private String version;
    private String extract;

    public String getArtifactId() {
        return artifactId;
    }

    public SpringProjectEntity setArtifactId(String artifactId) {
        this.artifactId = artifactId;
        return this;
    }

    public String getBootVersion() {
        return bootVersion;
    }

    public SpringProjectEntity setBootVersion(String bootVersion) {
        this.bootVersion = bootVersion;
        return this;
    }

    public String getBuild() {
        return build;
    }

    public SpringProjectEntity setBuild(String build) {
        this.build = build;
        return this;
    }

    public String getDependencies() {
        return dependencies;
    }

    public SpringProjectEntity setDependencies(String dependencies) {
        this.dependencies = dependencies;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public SpringProjectEntity setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getForce() {
        return force;
    }

    public SpringProjectEntity setForce(String force) {
        this.force = force;
        return this;
    }

    public String getFormat() {
        return format;
    }

    public SpringProjectEntity setFormat(String format) {
        this.format = format;
        return this;
    }

    public String getGroupId() {
        return groupId;
    }

    public SpringProjectEntity setGroupId(String groupId) {
        this.groupId = groupId;
        return this;
    }

    public String getJavaVersion() {
        return javaVersion;
    }

    public SpringProjectEntity setJavaVersion(String javaVersion) {
        this.javaVersion = javaVersion;
        return this;
    }

    public String getLanguage() {
        return language;
    }

    public SpringProjectEntity setLanguage(String language) {
        this.language = language;
        return this;
    }

    public String getPackaging() {
        return packaging;
    }

    public SpringProjectEntity setPackaging(String packaging) {
        this.packaging = packaging;
        return this;
    }

    public String getPackageName() {
        return packageName;
    }

    public SpringProjectEntity setPackageName(String packageName) {
        this.packageName = packageName;
        return this;
    }

    public String getType() {
        return type;
    }

    public SpringProjectEntity setType(String type) {
        this.type = type;
        return this;
    }

    public String getTarget() {
        return target;
    }

    public SpringProjectEntity setTarget(String target) {
        this.target = target;
        return this;
    }

    public String getVersion() {
        return version;
    }

    public SpringProjectEntity setVersion(String version) {
        this.version = version;
        return this;
    }

    public String getExtract() {
        return extract;
    }

    public SpringProjectEntity setExtract(String extract) {
        this.extract = extract;
        return this;
    }

    @JsonIgnore
    public String generateProjectParams() {
        setArtifactId(this.getArtifactId().toLowerCase());
        if (StringUtils.isEmpty(this.getName()))
            setName(String.valueOf(this.getArtifactId().toLowerCase().charAt(0)) + this.getArtifactId().substring(1).toLowerCase());
        StringBuilder params = new StringBuilder().append("-d ").append("name=").append(this.getName()).append(" ").append("-d baseDir=generated_project/").append(this.getName().toLowerCase()).append(" ");
        if (!StringUtils.isEmpty(this.getArtifactId()))
            params.append("-d ").append("artifactId=").append(this.getArtifactId()).append(" ");
        if (!StringUtils.isEmpty(this.getBootVersion()))
            params.append("-d ").append("bootVersion=").append(this.getBootVersion()).append(" ");
        if (!StringUtils.isEmpty(this.getBuild()))
            params.append("-d ").append("type=").append(this.getBuild()).append(" ");
        if (!StringUtils.isEmpty(this.getPackageName()))
            params.append("-d ").append("packageName=").append(this.getPackageName()).append(" ");
        if (!StringUtils.isEmpty(this.getJavaVersion()))
            params.append("-d ").append("javaVersion=").append(this.getJavaVersion()).append(" ");
        if (!StringUtils.isEmpty(this.getPackaging()))
            params.append("-d ").append("packaging=").append(this.getPackaging()).append(" ");
        if (!StringUtils.isEmpty(this.getGroupId()))
            params.append("-d ").append("groupId=").append(this.getGroupId()).append(" ");
        if (!StringUtils.isEmpty(this.getDependencies()))
            params.append("-d ").append("dependencies=").append(this.getDependencies()).append(" ");
        if (!StringUtils.isEmpty(this.getVersion()))
            params.append("-d ").append("version=").append(this.getVersion()).append(" ");
        return params.toString();
    }
}
