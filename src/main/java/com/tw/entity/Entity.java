package com.tw.entity;

public class Entity {

    private String javaPackage;
    private String className;
    private String superclass;
    private boolean constructors;
    private String javaFilePath;

    public String getJavaPackage() {
        return javaPackage;
    }

    public void setJavaPackage(String javaPackage) {
        this.javaPackage = javaPackage;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getSuperclass() {
        return superclass;
    }

    public void setSuperclass(String superclass) {
        this.superclass = superclass;
    }

    public boolean isConstructors() {
        return constructors;
    }

    public void setConstructors(boolean constructors) {
        this.constructors = constructors;
    }

    public String getJavaFilePath() {
        return javaFilePath;
    }

    public Entity setJavaFilePath(String javaFilePath) {
        this.javaFilePath = javaFilePath;
        return this;
    }
}
