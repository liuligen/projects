package com.tw.entity;

/**
 * Created by ligen on 2017/11/7.
 */
public class RepositoryEntry {

    private String projectName;
    private String gitRepositoryType;

    public RepositoryEntry(ProjectEntity projectEntity){
        this.projectName = projectEntity.getName();
        this.gitRepositoryType = projectEntity.getGitRepositoryType().name();
    }

    public String getProjectName() {
        return projectName;
    }

    public RepositoryEntry setProjectName(String projectName) {
        this.projectName = projectName;
        return this;
    }

    public String getGitRepositoryType() {
        return gitRepositoryType;
    }

    public RepositoryEntry setGitRepositoryType(String gitRepositoryType) {
        this.gitRepositoryType = gitRepositoryType;
        return this;
    }
}
