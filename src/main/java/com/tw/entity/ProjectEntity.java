package com.tw.entity;

import com.fasterxml.jackson.annotation.JsonSetter;
import com.tw.enumeration.GitRepositoryType;
import com.tw.enumeration.ProjectType;

import java.time.LocalDateTime;

//@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="@class")
public class ProjectEntity {

    protected ProjectType projectType;

    protected GitRepositoryType gitRepositoryType;

    protected LocalDateTime createDateTime;

    protected String name;

    public ProjectType getProjectType() {
        return projectType;
    }

    @JsonSetter
    public ProjectEntity setProjectType(ProjectType projectType) {
        this.projectType = projectType;
        return this;
    }

    public LocalDateTime getCreateDateTime() {
        return createDateTime;
    }

    @JsonSetter
    public ProjectEntity setCreateDateTime(LocalDateTime createDateTime) {
        this.createDateTime = createDateTime;
        return this;
    }

    public String getName() {
        return name;
    }

    @JsonSetter
    public ProjectEntity setName(String name) {
        this.name = name;
        return this;
    }

    public GitRepositoryType getGitRepositoryType() {
        return gitRepositoryType;
    }

    @JsonSetter
    public ProjectEntity setGitRepositoryType(GitRepositoryType gitRepositoryType) {
        this.gitRepositoryType = gitRepositoryType;
        return this;
    }
}
