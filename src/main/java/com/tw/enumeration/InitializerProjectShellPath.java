package com.tw.enumeration;

public enum InitializerProjectShellPath {

    SPRING_SHELL_PATH("shell_script/initializr_spring_project.sh"),
    INIT_GIT_LAB_SHELL_PATH("shell_script/git_init.sh"),
    COMPRESS_PROJECT_SHELL_PATH("shell_script/compress_project.sh");

    String shellPath;
    InitializerProjectShellPath(String shellPath){
        this.shellPath = shellPath;
    }

    public String getShellPath() {
        return shellPath;
    }
}
