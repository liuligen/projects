package com.tw.ddd.persistance.project

import com.tw.ddd.domain.project.model.Project
import com.tw.ddd.domain.project.model.ProjectId
import com.tw.ddd.domain.project.model.ProjectRepository
import com.tw.ddd.domain.project.model.SpringProject

import javax.persistence.EntityTransaction
import javax.persistence.Query;

import org.springframework.stereotype.Repository

import javax.persistence.EntityManager
import javax.persistence.PersistenceContext

@Repository
class ProjectRepositoryJpa implements ProjectRepository {

    @PersistenceContext
    private EntityManager entityManager




    @Override
    ProjectId nextId() {
        def rawId = UUID.randomUUID().toString().replaceAll('-', '')
        ProjectId.valueOf(rawId)
    }

    @Override
    Optional<Project> of(ProjectId id) {
        Optional.ofNullable(entityManager.find(Project, id))
    }

    void store(Project project) {
        if (null == entityManager.find(Project, project.id)) {
            if(project instanceof SpringProject)
                entityManager.persist((SpringProject)project)

        }
        else {
            entityManager.merge(project)
        }
    }

    List<Project> findAll(){
        final String queryString = "select p from Project p"
        Query query = entityManager.createQuery(queryString)
        return query.getResultList()
    }

}
