package com.tw.ddd.domain.project.model

enum ProjectType {
    JAVA,
    GROOVY,
    KOTLIN,
    SCALA,
}
