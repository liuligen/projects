package com.tw.ddd.domain.project.model

interface ProjectRepository {

    ProjectId nextId()

    Optional<Project> of(ProjectId id)

    void store(Project project)

}
