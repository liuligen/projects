package com.tw.ddd.domain.project.model

import com.tw.util.LocalDateTimeConverter
import groovy.transform.builder.Builder

import javax.persistence.*
import java.time.LocalDateTime

@Builder
@Entity
@Table(name = 'SPRING_PROJECTS')
@PrimaryKeyJoinColumn(name = "project_id")
class SpringProject extends Project {

    @Column(name = 'boot_version')
    String bootVersion

    @Column(name = 'java_version')
    String javaVersion



}
