package com.tw.ddd.domain.project.model

import groovy.transform.EqualsAndHashCode
import groovy.transform.builder.Builder

import javax.persistence.Column
import javax.persistence.Embeddable

@Builder
@EqualsAndHashCode(includes = ['value'])
@Embeddable
class ProjectId implements Serializable {

    @Column(name = 'id')
    String value

    String asString() {
        value
    }

    static ProjectId valueOf(String value) {
        builder().value(value).build()
    }

}
