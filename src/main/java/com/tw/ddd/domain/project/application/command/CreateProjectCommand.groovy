package com.tw.ddd.domain.project.application.command

import com.tw.ddd.domain.project.model.ProjectType
import groovy.transform.builder.Builder

@Builder
class CreateProjectCommand {

    String name

    ProjectType type

    String bootVersion

    String javaVersion

    CreateProjectCommand(String name, ProjectType type, String bootVersion, String javaVersion) {
        this.name = name
        this.type = type
        this.bootVersion = bootVersion
        this.javaVersion = javaVersion
    }
}
