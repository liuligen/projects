package com.tw.ddd.domain.project.application

import com.tw.ddd.domain.project.application.command.CreateProjectCommand
import com.tw.ddd.domain.project.model.Project
import com.tw.ddd.domain.project.model.ProjectId
import com.tw.ddd.domain.project.model.ProjectRepository
import com.tw.ddd.domain.project.model.ProjectStatus
import com.tw.ddd.domain.project.model.SpringProject
import com.tw.ddd.domain.project.model.SubprojectType
import com.tw.ddd.domain.project.service.ProjectService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional

import java.time.LocalDateTime

@Service
class ProjectApplicationService {

    private final ProjectRepository projectRepository
    private final ProjectService projectService

    @Autowired
    ProjectApplicationService(ProjectRepository projectRepository,
                              ProjectService projectService) {
        this.projectRepository = projectRepository
        this.projectService = projectService
    }

    @Transactional
    Project createProject(CreateProjectCommand command) {
        def currentTime = LocalDateTime.now()
        def project = Project.builder()
                .id(projectRepository.nextId())
                .name(command.name)
                .type(command.type)
                .status(ProjectStatus.INITIALIZING)
                .subprojectType(SubprojectType.SPRING)
                .createdAt(currentTime)
                .build()
        def springProject = SpringProject.builder()
                .bootVersion(command.bootVersion)
                .javaVersion(command.javaVersion)
                .build()
        springProject.setProject(project)
        projectRepository.store(springProject)
        project
    }

    InputStream generateTemplate(ProjectId id) {
        throw new UnsupportedOperationException()
    }

}
