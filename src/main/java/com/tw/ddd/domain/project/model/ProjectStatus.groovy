package com.tw.ddd.domain.project.model

enum ProjectStatus {
    CREATING,
    INITIALIZING,
    INITIALIZED,
}