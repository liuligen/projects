package com.tw.ddd.domain.project.service

import com.tw.ddd.domain.project.model.Project
import com.tw.ddd.domain.project.model.ProjectRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.scheduling.annotation.Async
import org.springframework.stereotype.Service

@Service
class ProjectService {

    private final ProjectRepository projectRepository

    @Autowired
    ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository
    }

    @Async
    void initProject(Project project) {
        // create git repo -> generate code template
    }

    List<Project> findAll(){
        return projectRepository.findAll();
    }

}
