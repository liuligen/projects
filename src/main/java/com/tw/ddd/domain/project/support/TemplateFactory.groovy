package com.tw.ddd.domain.project.support

import org.springframework.scheduling.annotation.Async

import java.util.concurrent.CompletableFuture

class TemplateFactory {

    @Async
    CompletableFuture<byte[]> createSpringProjectTemplate() {
        throw new UnsupportedOperationException()
    }

}
