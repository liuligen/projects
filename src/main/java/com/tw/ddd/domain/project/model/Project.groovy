package com.tw.ddd.domain.project.model

import com.tw.util.LocalDateTimeConverter
import groovy.transform.builder.Builder
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters

import javax.persistence.*
import java.time.LocalDateTime

@Builder
@Entity
@Table(name = 'SCD_PROJECTS')
@Inheritance(strategy=InheritanceType.JOINED)
//@DiscriminatorColumn(name="subproject_type", discriminatorType=DiscriminatorType.STRING)
class Project {

    @EmbeddedId
    ProjectId id
//    @Id
//    @Column(name = 'id')
//    @GeneratedValue
//    String id

    @Column(name = 'type')
    @Enumerated(EnumType.STRING)
    ProjectType type

    @Column(name = 'status')
    @Enumerated(EnumType.STRING)
    ProjectStatus status

    @Column(name = 'subproject_type')
    @Enumerated(EnumType.STRING)
    SubprojectType subprojectType

    @Column(name = 'name')
    String name

    @Convert(converter = LocalDateTimeConverter.class)
    @Column(name = 'created_at')
    LocalDateTime createdAt

    @Convert(converter = LocalDateTimeConverter.class)
    @Column(name = 'updated_at')
    LocalDateTime updatedAt

    void initComplete() {
        this.status = ProjectStatus.INITIALIZED
    }

    InputStream generateTemplate() {
        throw new UnsupportedOperationException()
    }

    void setProject(Project project){
        this.setId(project.getId())
        this.setType(project.getType())
        this.setStatus(project.getStatus())
        this.setSubprojectType(project.getSubprojectType())
        this.setName(project.getName())
        this.setCreatedAt(project.getCreatedAt())
    }

}
