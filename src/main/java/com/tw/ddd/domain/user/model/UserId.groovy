package com.tw.ddd.domain.user.model

import groovy.transform.EqualsAndHashCode
import groovy.transform.builder.Builder

@Builder
@EqualsAndHashCode(includes = ['value'])
class UserId {

    String value

    static UserId valueOf(String value) {
        builder().value(value).build()
    }

}
