package com.tw.ddd.resource.project.request

import groovy.transform.builder.Builder

@Builder
class CreateProjectRequest {

    String name

    String type

}
