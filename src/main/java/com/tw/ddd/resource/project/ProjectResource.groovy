package com.tw.ddd.resource.project

import com.tw.ddd.domain.project.application.ProjectApplicationService
import com.tw.ddd.domain.project.application.command.CreateProjectCommand
import com.tw.ddd.domain.project.model.ProjectId
import com.tw.ddd.domain.project.model.ProjectType
import com.tw.ddd.resource.project.request.CreateProjectRequest
import com.tw.ddd.resource.project.response.CreateProjectResponse
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping('/projects_')
class ProjectResource {

    private final ProjectApplicationService projectApplicationService

    @Autowired
    ProjectResource(ProjectApplicationService projectApplicationService) {
        this.projectApplicationService = projectApplicationService
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    CreateProjectResponse createProject(@RequestBody CreateProjectRequest request) {
        def createProjectCommand = CreateProjectCommand.builder()
                .name(request.name)
                .type(ProjectType.valueOf(request.type))
                .build()
        def createdProject = projectApplicationService.createProject(createProjectCommand)
        CreateProjectResponse.builder()
                .id(createdProject.id.asString())
                .build()
    }

    @GetMapping('/{id}/template')
    ResponseEntity<byte[]> downloadTemplate(@PathVariable('id') String id) {
        def inputStream = projectApplicationService.generateTemplate(ProjectId.valueOf(id))
        ResponseEntity.ok(inputStream.getBytes())
    }

    @GetMapping('/{id}')
    ResponseEntity<?> getProject() {
        throw new UnsupportedOperationException()
    }

}
