package com.tw.ddd.resource.project.response

import groovy.transform.builder.Builder

@Builder
class CreateProjectResponse {
    String id
}
