package com.tw.api;

import com.tw.ddd.domain.project.application.ProjectApplicationService;
import com.tw.ddd.domain.project.application.command.CreateProjectCommand;
import com.tw.ddd.domain.project.model.Project;
import com.tw.ddd.domain.project.service.ProjectService;
import com.tw.entity.SpringProjectEntity;
import com.tw.service.CreateProject;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

@RestController
public class ProjectAPI {

    private final CreateProject createProject;

    private final ProjectApplicationService projectApplicationService;

    private final ProjectService projectService;

    @Autowired
    public ProjectAPI(CreateProject createProject, ProjectApplicationService projectApplicationService, ProjectService projectService) {
        this.createProject = createProject;
        this.projectApplicationService = projectApplicationService;
        this.projectService = projectService;
    }

    @PostMapping(value = "/project/{id}/build")
    public ResponseEntity<?> buildProject() {
//        return ResponseEntity.created();
        throw new UnsupportedOperationException();
    }

    @RequestMapping(value = "/projects", method = {RequestMethod.POST})
    public ResponseEntity<byte[]> generateProject(@RequestBody SpringProjectEntity projectEntity) throws IOException {
        String projectZipName = createProject.createProject(projectEntity);
        CreateProjectCommand createProjectCommand = new CreateProjectCommand(projectEntity.getArtifactId(), com.tw.ddd.domain.project.model.ProjectType.JAVA,
                projectEntity.getBootVersion(), projectEntity.getJavaVersion());
        projectApplicationService.createProject(createProjectCommand);
        return download(projectZipName);
    }

    public ResponseEntity<byte[]> download(String fileName) throws IOException {
        File file = new File("generated_project/" + fileName);
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData("attachment", fileName);
        headers.setAccessControlExposeHeaders(Collections.singletonList("Content-Disposition"));

//        InputStream is = new FileInputStream("generated_project/rairport.tar.gz");
//        int read =0;
//        byte[] bytes = new byte[2048];
//        OutputStream os = response.getOutputStream();
//        while((read = is.read(bytes))!=-1){//按字节逐个写入，避免内存占用过高
//            os.write(bytes, 0, read);
//        }
//        os.flush();
//        os.close();
//        is.close();

        return ResponseEntity
                .status(HttpStatus.CREATED)
                .headers(headers)
                .body(FileUtils.readFileToByteArray(file));
    }

    @RequestMapping(value = "/projects", method = {RequestMethod.GET})
    public ResponseEntity<List<Project>> home() {
        List<Project> allProjects = projectService.findAll();

        return ResponseEntity
                .status(HttpStatus.OK)
                .body(allProjects);
    }

}
