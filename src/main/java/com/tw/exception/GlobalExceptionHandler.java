package com.tw.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler({RequestException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorInfo<String> jsonErrorHandler(HttpServletRequest req, RequestException e) throws Exception {
        ErrorInfo<String> errorInfo = new ErrorInfo<>();
        errorInfo.setMessage(e.getErrorCode().name());
        errorInfo.setCode(ErrorInfo.ERROR);
        errorInfo.setErrorCode(e.getErrorCode().name());
        errorInfo.setData("");
        errorInfo.setUrl(req.getRequestURL().toString());
        return errorInfo;
    }
}
