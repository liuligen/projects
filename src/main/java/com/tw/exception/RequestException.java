package com.tw.exception;

import org.springframework.http.HttpStatus;

import java.util.HashMap;
import java.util.Map;

public class RequestException extends RuntimeException {

    private final HttpStatus status;
    private final ErrorCode errorCode;
    private final Map<String, String> errorMessages;

    public RequestException(ErrorCode errorCode) {
        this(errorCode.getStatus(), errorCode, new HashMap<>());
    }

    protected RequestException(HttpStatus status, ErrorCode errorCode, Map<String, String> errorMessages) {
        this.status = status;
        this.errorCode = errorCode;
        this.errorMessages = errorMessages;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public Map<String, String> getErrorMessages() {
        return errorMessages;
    }
}
