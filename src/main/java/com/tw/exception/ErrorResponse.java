package com.tw.exception;

public class ErrorResponse {

    protected String errorCode;

    public ErrorResponse(String errorCode){
        this.errorCode =  errorCode;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

}
