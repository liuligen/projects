package com.tw.exception;

import org.springframework.http.HttpStatus;

public enum ErrorCode {

    // * 0 ~ 10000, system error
    UNKNOWN(0, HttpStatus.BAD_REQUEST, "系统未知错误", "System unknown error."),
    SYSTEM_ERROR(1, HttpStatus.INTERNAL_SERVER_ERROR, "系统错误", "System error."),
    DATABASE_ERROR(2, HttpStatus.INTERNAL_SERVER_ERROR, "数据库错误", "DataBase error."),
    DATA_NOT_CONSISTENT(3, HttpStatus.INTERNAL_SERVER_ERROR, "数据不一致错误", "Data not consistent error."),
    NOT_PERMITTED(4, HttpStatus.FORBIDDEN, "不允许访问", "Request not permitted"),
    UNAUTHORIZED(5, HttpStatus.NOT_ACCEPTABLE, "未授权的访问", "Request Unauthorized"),
    ILLEGAL_REQUEST(6, HttpStatus.BAD_REQUEST, "非法请求", "Illegal request"),
    FILTER_REQUEST_BODY_ERROR(7, HttpStatus.BAD_REQUEST, "请求内容处理异常", "deal with request object error.")
    ;

    private final int code;

    private final HttpStatus status;

    private final String cnMsg;

    private final String enMsg;

    ErrorCode(int code, HttpStatus status, String cnMsg, String enMsg) {
        this.code = code;
        this.status = status;
        this.cnMsg = cnMsg;
        this.enMsg = enMsg;
    }

    public int getCode() {
        return code;
    }

    public HttpStatus getStatus() {
        return status;
    }

    public String getCnMsg() {
        return cnMsg;
    }

    public String getEnMsg() {
        return enMsg;
    }

    @Override
    public String toString() {
        return "ErrorCode{" +
                "code=" + code +
                ", status=" + status +
                ", cnMsg='" + cnMsg + '\'' +
                ", enMsg='" + enMsg + '\'' +
                '}';
    }
}
