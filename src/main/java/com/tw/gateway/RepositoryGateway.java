package com.tw.gateway;

import com.tw.entity.RepositoryEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.AsyncRestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
public class RepositoryGateway {

    @Value("${repository.service.url}")
    private String repositoryServiceUrl;

    private final String INIT_RESPOSITORY_URL = "/initRepository";

    Logger logger = LoggerFactory.getLogger(RepositoryGateway.class);

    public void asyncInitRepository(RepositoryEntry repositoryEntry) {

        MultiValueMap<String, String> map= new LinkedMultiValueMap<String, String>();
        map.add("name", repositoryEntry.getProjectName());
        map.add("getRepositoryType", repositoryEntry.getGitRepositoryType());
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<MultiValueMap<String, String>>(map, headers);


        try {
            AsyncRestTemplate asyncRestTemplate = new AsyncRestTemplate();
            new Thread(() -> {
                asyncRestTemplate.postForEntity(repositoryServiceUrl + INIT_RESPOSITORY_URL, request, ResponseEntity.class);
            }).start();
        } catch (Exception e) {
            logger.error("async campaign to data service failed {}", repositoryEntry, e);
        }
    }

}
