package com.tw.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import ch.qos.logback.core.util.FileUtil;
import com.tw.entity.Entity;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.util.FileSystemUtils;

public class GenerateFile {

    public static void copyProject(String[] envp) {
        runShell(envp);
    }

    public static void runShell(String[] envp) {
        String shpath = "shell_script/copy_project_dir.sh";
        Resource resource = new ClassPathResource(shpath);

        try {
            String absolutePath = resource.getFile().getAbsolutePath();
            String absoluteParentDirPath = absolutePath.substring(0, absolutePath.lastIndexOf("/shell_script"));
            Process ps = Runtime.getRuntime().exec(new String[]{"/bin/sh", "-c", absolutePath + " " + absoluteParentDirPath + " airport"});
            ps.waitFor();

            BufferedReader br = new BufferedReader(new InputStreamReader(ps.getInputStream()));
            StringBuffer sb = new StringBuffer();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }
            String result = sb.toString();
            System.out.println(result);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private static File javaFile = null;

    public static void generateApplicationJavaFile(Entity entity) {
        String rootPath = "generated_project/" + entity.getClassName().toLowerCase() + "/src/main/java/";
        String rootTestPath = "generated_project/" + entity.getClassName().toLowerCase() + "/src/test/java/";
        new File(rootPath + entity.getJavaFilePath()).mkdirs();
        new File(rootTestPath + entity.getJavaFilePath()).mkdirs();

        Configuration cfg = new Configuration();
        try {
            // 步骤一：指定 模板文件从何处加载的数据源，这里设置一个文件目录
            cfg.setDirectoryForTemplateLoading(new File(rootPath + entity.getJavaFilePath()));
            cfg.setObjectWrapper(new DefaultObjectWrapper());

            // 步骤二：获取 模板文件
            Template template = cfg.getTemplate("Application.ftl");

            // 步骤三：创建 数据模型
            Map<String, Object> root = createDataModel(entity);

            // 步骤四：合并 模板 和 数据模型
            // 创建.java类文件
            if (javaFile != null) {
                Writer javaWriter = new FileWriter(javaFile);
                template.process(root, javaWriter);
                javaWriter.flush();
                System.out.println("文件生成路径：" + javaFile.getCanonicalPath());

                javaWriter.close();
            }
            // 输出到Console控制台
            Writer out = new OutputStreamWriter(System.out);
            template.process(root, out);
            out.flush();
            out.close();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }

    }

    private static Map<String, Object> createDataModel(Entity entity) {
        Map<String, Object> root = new HashMap<String, Object>();
        entity.setJavaPackage("com.tw"); // 创建包名
        entity.setClassName("Airport");  // 创建类名
        entity.setConstructors(false); // 是否创建构造函数


        // 创建.java类文件
        File outDirFile = new File("./src-template");
        if (!outDirFile.exists()) {
            outDirFile.mkdir();
        }

        javaFile = toJavaFilename(outDirFile, entity.getJavaPackage(), entity.getClassName());

        root.put("entity", entity);
        return root;
    }


    /**
     * 创建.java文件所在路径 和 返回.java文件File对象
     *
     * @param outDirFile    生成文件路径
     * @param javaPackage   java包名
     * @param javaClassName java类名
     * @return
     */
    private static File toJavaFilename(File outDirFile, String javaPackage, String javaClassName) {
        String packageSubPath = javaPackage.replace('.', '/');
        File packagePath = new File(outDirFile, packageSubPath);
        File file = new File(packagePath, javaClassName + ".java");
        if (!packagePath.exists()) {
            packagePath.mkdirs();
        }
        return file;
    }

}
