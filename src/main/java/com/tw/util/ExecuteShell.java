package com.tw.util;

import com.tw.exception.RequestException;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ExecuteShell {

    public static int executeShell(String shellPath, String shellScriptParams) throws RequestException {
        Resource resource = new ClassPathResource(shellPath);

        try {
            String absolutePath = resource.getFile().getAbsolutePath();
            Runtime.getRuntime().exec("chmod 777 " + absolutePath);
            Process ps = Runtime.getRuntime().exec(new String[]{absolutePath, shellScriptParams});
            int executeValue = ps.waitFor();

            BufferedReader br = new BufferedReader(new InputStreamReader(ps.getInputStream()));
            BufferedReader er = new BufferedReader(new InputStreamReader(ps.getErrorStream()));

            StringBuffer sb = new StringBuffer();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }
            System.out.println("ERROR MESSAGE:");
            while ((line = er.readLine()) != null) {
                System.out.println(line);
            }
            System.out.println("ERROR MESSAGE END");

            String result = sb.toString();
            System.out.println(result);
            ps.destroy();
            return executeValue;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }

}
