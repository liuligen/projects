package com.tw.service;

import com.tw.entity.ProjectEntity;
import com.tw.enumeration.InitializerProjectShellPath;
import com.tw.exception.ErrorCode;
import com.tw.exception.RequestException;
import com.tw.factory.CreateProjectCommand;
import com.tw.factory.VcsRepository;
import com.tw.factory.GitRepositoryFactory;
import com.tw.factory.ProjectGenerate;
import com.tw.factory.ProjectGenerateFactory;
import com.tw.util.ExecuteShell;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CreateProject {

    private final ProjectGenerateFactory projectGenerateFactory;

    private final GitRepositoryFactory gitRepositoryFactory;

    @Autowired
    public CreateProject(ProjectGenerateFactory projectGenerateFactory, GitRepositoryFactory gitRepositoryFactory) {
        this.projectGenerateFactory = projectGenerateFactory;
        this.gitRepositoryFactory = gitRepositoryFactory;
    }

    public String createProject(ProjectEntity projectEntity) {
        List<CreateProjectCommand> commandList = new ArrayList<>();
        ProjectGenerate projectGenerate = projectGenerateFactory.getProjectGenerate(projectEntity);
        VcsRepository gitRepository = gitRepositoryFactory.getGitRepository(projectEntity);
        commandList.add(projectGenerate);
        commandList.add(gitRepository);
        doCreateProject(commandList, projectEntity);

        return generateProjectZipName(projectEntity.getName());
    }

    private void doCreateProject(List<CreateProjectCommand> commandList, ProjectEntity projectEntity) {
        for (CreateProjectCommand command : commandList) {
            command.execute(projectEntity);
        }
        compressProject(projectEntity.getName());
    }

    private void compressProject(String projectName) {
        int exitValue = ExecuteShell.executeShell(InitializerProjectShellPath.COMPRESS_PROJECT_SHELL_PATH.getShellPath(), projectName);
        if (0 != exitValue) {
            throw new RequestException(ErrorCode.SYSTEM_ERROR);
        }
    }

    private String generateProjectZipName(String projectName) {
        String suffix = ".tar.gz";
        return projectName + suffix;
    }
}
