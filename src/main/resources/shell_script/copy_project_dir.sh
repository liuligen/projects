#!/bin/sh
echo Begin Copy
parentPath=$1
projectName=$2
pwd
cp -r ${parentPath}/templates/gradle generated_project/${projectName}

echo End Copy