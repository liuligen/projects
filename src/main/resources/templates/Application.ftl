package ${entity.javaPackage};

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ${entity.className}Application {

	public static void main(String[] args) {
		SpringApplication.run(${entity.className}Application.class, args);
	}
}
